export function throttle(func: Function, wait: number) {
  let context: any, args: any, result: any;
  let timeout: any = null;
  let previous = 0;
  const later = function() {
    previous = Date.now();
    timeout = null;
    result = func.apply(context, args);
    if (!timeout) context = args = null;
  };
  return function() {
    const now = Date.now();
    if (!previous) previous = now;
    const remaining = wait - (now - previous);
    // @ts-ignore: TS complaining about this: any;
    context = this;
    args = arguments;
    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = now;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    } else if (!timeout) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
}

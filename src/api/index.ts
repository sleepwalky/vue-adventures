import { RepositoriesPayload } from './types';

export const fetchRepositories = (query: string, currentPage = 0) => {
  return fetch(
    `https://api.github.com/search/repositories?q=${query}&sort=stars&page=${currentPage}`
  )
    .then(resp => resp.json())
    .then(data => {
      const results = data as RepositoriesPayload;
      return results.items;
    });
};

import Vue from 'vue';
import Vuex from 'vuex';
import { fetchRepositories } from './api';
import { State } from './types';
import { Item } from './api/types';

Vue.use(Vuex);

const initialState: State = {
  repos: [],
  query: '',
  currentPage: 0,
  isLoading: false
};

export default new Vuex.Store({
  state: initialState,
  actions: {
    FETCH_DATA: ({ commit, dispatch, state }, { query }) => {
      commit('SET_QUERY', { query });
      commit('SET_IS_LOADING', { isLoading: true });
      return fetchRepositories(query).then(items => {
        commit('SET_REPOSITORIES', { items });
        commit('SET_IS_LOADING', { isLoading: false });
      });
    },
    FETCH_NEXT_PAGE: ({ commit, dispatch, state }) => {
      commit('SET_IS_LOADING', { isLoading: true });
      commit('SET_NEXT_PAGE');
      return fetchRepositories(state.query, state.currentPage).then(items => {
        commit('SET_REPOSITORIES', { items });
        commit('SET_IS_LOADING', { isLoading: false });
      });
    }
  },
  mutations: {
    SET_QUERY: (state, { query }) => {
      if (state.query !== query) {
        state.query = query;
        Vue.set(state, 'repos', []);
        state.currentPage = 0;
      }
    },
    SET_REPOSITORIES: (state, { items }) => {
      Vue.set(state, 'repos', [...state.repos, ...(items as Item[])]);
    },
    SET_IS_LOADING: (state, { isLoading }) => {
      state.isLoading = isLoading;
    },
    SET_NEXT_PAGE: state => {
      state.currentPage += 1;
    }
  }
});

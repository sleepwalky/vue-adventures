import { Item } from './api/types';

export type State = {
  repos: Item[],
  query: string,
  currentPage: number,
  isLoading: boolean,
}

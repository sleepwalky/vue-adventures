module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/vue-adventures/'
    : '/',
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/scss/_variables.scss";
        `
      }
    }
  }
};

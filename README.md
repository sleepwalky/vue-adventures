The task:
Develop a small app that shows a list of all JavaScript repos on Github, sorted by number of stars.

Requirements:
* List’s columns: name, url (link), owner, forks, open issues, watchers.
* Should have pagination on scroll
* Should show loader when data is being loaded.
* Should look nice and have some styling

Note:
* You can use GitHub API for that (https://api.github.com)
* Use Vue.js. Any flavor you’re comfortable with (ts, jsx, scss, scoped, bem, etc).
* You can use any other libraries.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
